/*
   SPDX-FileCopyrightText: 2013-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef FOLDERARCHIVEUTIL_H
#define FOLDERARCHIVEUTIL_H

#include <QString>
namespace FolderArchive {
namespace FolderArchiveUtil {
QString groupConfigPattern();
QString configFileName();
}
}

#endif // FOLDERARCHIVEUTIL_H
