/*
 * SPDX-FileCopyrightText: 2012 Christian Mollekopf <mollekopf@kolabsys.com>
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#ifndef KOLABDATETIMEUTILS_H
#define KOLABDATETIMEUTILS_H

#include "kolab_export.h"

#include <string>

namespace Kolab {
namespace DateTimeUtils {
KOLAB_EXPORT std::string getLocalTimezone();
}     //Namespace
} //Namespace

#endif
